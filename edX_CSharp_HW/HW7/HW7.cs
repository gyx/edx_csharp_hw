﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edX_CSharp_HW.HW7 {
    class HW7 {
        static void Main(string[] args) {/*HW7
            1) Delete the Student array in your Course object that you created in Module 5.
            2) Create an ArrayList to replace the array and to hold students, inside the Course object.
            3) Modify your code to use the ArrayList collection as the replacement to the array.  In other words, when you add a Student to the Course object, you will add it to the ArrayList and not an array.
            4) Create a Stack object inside the Student object, called Grades, to store test scores.
            5) Create 3 student objects.
            6) Add 5 grades to the the Stack in the each Student object. (this does not have to be inside the constructor because you may not have grades for a student when you create a new student.)
            7) Add the three Student objects to the Students ArrayList inside the Course object.
            8) Using a foreach loop, iterate over the Students in the ArrayList and output their first and last names to the console window. (For this exercise you MUST cast the returned object from the ArrayList to a Student object.  Also, place each student name on its own line)
            9) Create a method inside the Course object called ListStudents() that contains the foreach loop.
            10) Call the ListStudents() method from Main().
            
             * Grading Criteria:
            1) Used an ArrayList of type Student, inside the Course object.
            2) Added a Stack called Grades inside the Student object.
            3) Added 3 Student objects to this ArrayList using the ArrayList method for adding objects.
            4) Used a foreach loop to output the first and last name of each Student in the ArrayList.
            5) Cast the object from the ArrayList to Student, inside the foreach loop, before printing out the name information.*/
            
            Student S1 = new Student("Student1name", "Student1surname", "2001-01-01");
            Student S2 = new Student("Student2name", "Student2surname", "2002-02-02");
            Student S3 = new Student("Student3name", "Student3surname", "2003-03-03");

            //Adding grades
            S1.Grades.Push("A");
            S1.Grades.Push("B");
            S1.Grades.Push("C");
            S1.Grades.Push("D");
            S1.Grades.Push("E");

            S2.Grades.Push("A");
            S2.Grades.Push("B");
            S2.Grades.Push("C");
            S2.Grades.Push("D");
            S2.Grades.Push("E");

            S3.Grades.Push("A");
            S3.Grades.Push("B");
            S3.Grades.Push("C");
            S3.Grades.Push("D");
            S3.Grades.Push("E");

            Course course = new Course("Programming with C#", "edX", "DEV204x", 10, 400M);

            //Adding to ArrayList
            course.students.Add(S1);
            course.students.Add(S2);
            course.students.Add(S3);

            Teacher T1 = new Teacher("Teacher1name", "Teacher1surname", "Teacher1college", "Teacher1email", "Teacher1phone");

            course.teachers = new Teacher[] { T1 };

            Degree degree = new Degree("The Bachelor of Science degree", "BS", 30);

            degree.courses = new Course[] { course };

            UProgram IT = new UProgram("The Information technology program");

            IT.degrees = new Degree[] { degree };

            //output uses overrided ToString method that exists in every Object
            Console.WriteLine(IT);
            Console.WriteLine();

            Console.WriteLine(IT.degrees[0]);
            Console.WriteLine();

            Console.WriteLine(IT.degrees[0].courses[0]);
            Console.WriteLine();

            //Student list output
            IT.degrees[0].courses[0].getStudentList();
            Console.WriteLine();
        }
    }
}
