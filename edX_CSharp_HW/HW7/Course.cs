﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edX_CSharp_HW.HW7 {
    class Course {
        public string courseName { get; set; }
        public string collegeName { get; set; }
        public string courseAbbreviation { get; set; }
        public int courseCredits { get; set; }
        public decimal courseCost { get; set; }
        //added students
        public ArrayList students = new ArrayList();
        public Teacher[] teachers { get; set; }

        public Course(string cName, string colName, string cAbb, int cCred, decimal cCost) {
            courseName = cName;
            collegeName = colName;
            courseAbbreviation = cAbb;
            courseCredits = cCred;
            courseCost = cCost;
        }

        public override string ToString() {
            return courseName + " contains " + students.Count + " student(s) and " + teachers.Length + " teacher(s)";
        }

        //student list output
        public void getStudentList() {
            Console.WriteLine(courseName + " student list:");
            int i = 1;
            foreach (Student student in students) {//Cast the object from the ArrayList to Student
                Console.WriteLine(i + ") First Name: " + student.firstName + " Last Name: " + student.lastName);
                i++;
            }
        }
    }
}
