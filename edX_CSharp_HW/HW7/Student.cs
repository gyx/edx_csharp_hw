﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edX_CSharp_HW.HW7 {
    class Student : Person {
        //added grades
        public Stack Grades = new Stack();

        public Student(string fName, string lName, string bDate) : base(fName, lName, bDate) {

        }

        public string TakeTest() {
            Random rnd = new Random();

            int length = rnd.Next(1, 21);

            char [] test = new char[length];
            for ( int i = 0; i < length; i++ ) {
                test[i] = (char)rnd.Next ('a', 'z');
            }
            
            return test.ToString();
        }
    }
}
