﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
Prior to object oriented considerations and class files, programmers created structs in languages such as C.  Some programmers still use structs for storing related information, although the trend is more towards class files.  
Because there may still be occasions where a struct makes sense in your code, you're going to create some structs in this assignment.  
Because a struct has a similar layout to a class file, this will provide you with a layout for the variables in your student, teacher, program, and course aspects.

For this assignment, complete the following tasks.  For the structs, just include member variables and a constructor.  Do not create properties at this time.  Include all the variables that you have created up to this point in time.

Create a struct to represent a student
Create a struct to represent a teacher
Create a struct to represent a program
Create a struct to represent a course
Create an array to hold 5 student structs.
Assign values to the fields in at least one of the student structs in the array
Using a series of Console.WriteLine() statements, output the values for the student struct that you assigned in the previous step
When complete, submit your code in the peer review section.
*/

namespace edX_CSharp_HW {
    class HW4 {
        static string GetConsoleInput(string cmsg) {
            string cin;
            do {
                Console.WriteLine(cmsg);
                cin = Console.ReadLine();
            } while (cin.Equals(""));

            return cin;
        }

        static DateTime DateParse(string msg) {
            DateTime dt;
            string str;
            do {
                str = GetConsoleInput(msg + " (yyyy.mm.dd, dd/mm/yy etc): ");
            } while (!DateTime.TryParse(str, out dt));

            return dt;
        }

        public struct Student {
            public bool exists { get; set; } //for debugging
            public string firstName { get; set; }
            public string lastName { get; set; }
            public DateTime birthDate { get; set; }

            public Student(string fName, string lName, string bDate) {
                this = new Student();
                exists = true;
                firstName = fName;
                lastName = lName;
                DateTime dt = new DateTime();

                try { 
                    if (!DateTime.TryParse(bDate, out dt)) throw new FormatException(); 
                }
                catch (FormatException fe) { 
                    Console.WriteLine(fe.Message + " Birth date was not succesfully parsed.");
                }

                birthDate = dt;
            }
        }

        static void PrintStudentDetails(Student S) {
            Console.WriteLine("{0} {1} was born on: {2}", S.firstName, S.lastName, S.birthDate.ToString("yyyy.MM.dd"));
            Console.WriteLine();
        }

        public struct Group {
            public bool exists { get; set; }
            public Student[] students; //array of our Student structs

            public Group(int num) {
                this = new Group();
                exists = true;
                students = new Student[num];
            }

            public Student this[int index] { //indexer
                get { return this.students[index]; }
                set { this.students[index] = value; }
            }

            public int Length { // Enable client code to determine the size of the collection.
                get { return students.Length; }
            }
        }

        static void PrintGroupDetails(Group G) {
            Console.WriteLine("Number of students in group is " + G.Length);
            Console.WriteLine();
            for (int i = 0; i < 5; i++) {
                PrintStudentDetails(G[i]);
            }
        }

        public struct Teacher {
            public bool exists { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string college { get; set; }
            public string email { get; set; }
            public string phone { get; set; }

            public Teacher(string fName, string lName, string coll, string mail, string ph) {
                this = new Teacher();
                exists = true;
                firstName = fName;
                lastName = lName;
                college = coll;
                email = mail;
                phone = ph;
            }
        }

        static void PrintTeacherDetails(Teacher T) {
            Console.WriteLine("{0} {1} from {2}, email: {3}, phone: {4}", T.firstName, T.lastName, T.college, T.email, T.phone);
            Console.WriteLine();
        }

        public struct Course {
            public bool exists { get; set; }
            public string courseName { get; set; }
            public string collegeName { get; set; }
            public string courseAbbreviation { get; set; }
            public int courseCredits { get; set; }
            public decimal courseCost { get; set; }

            public Course(string cName, string colName, string cAbb, int cCred, decimal cCost) {
                this = new Course();
                exists = true;
                courseName = cName;
                collegeName = colName;
                courseAbbreviation = cAbb;
                courseCredits = cCred;
                courseCost = cCost;
            }
        }

        static void PrintCourseDetails(Course C) {
            Console.WriteLine("{0} in {1} abbreviated {2} with {3} credits for as little as {4}", C.courseName, C.collegeName, C.courseAbbreviation, C.courseCredits, C.courseCost);
            Console.WriteLine();
        }

        public struct Degree {
            public bool exists { get; set; }
            public string degreeName { get; set; }
            public string degreeAbbreviation { get; set; }
            public int degreeCredits { get; set; }

            public Degree(string dName, string dAbb, int dCred) {
                this = new Degree();
                exists = true;
                degreeName = dName;
                degreeAbbreviation = dAbb;
                degreeCredits = dCred;
            }
        }

        static void PrintDegreeDetails(Degree D) {
            Console.WriteLine("Degree name: {0}, Abbreviation: {1}, Credits required: {2}", D.degreeName, D.degreeAbbreviation, D.degreeCredits);
            Console.WriteLine();
        }

        static void Main(string[] args) {
            //HW4 hw4 = new HW4(); //made methods static, so no need to instantiate an instance of a class
            //Student S = new Student();
            //S.firstName = GetConsoleInput("Enter name: ");
            //S.lastName = GetConsoleInput("Enter last name: ");
            //S.birthDate = DateParse("Enter birth date: ");

            Student S1 = new Student("Student1name", "Student1surname", "2001-01-01");
            Student S2 = new Student("Student2name", "Student2surname", "2002-02-02");
            Student S3 = new Student("Student3name", "Student3surname", "2003-03-03");
            Student S4 = new Student("Student4name", "Student4surname", "2004-04-04");
            Student S5 = new Student("Student5name", "Student5surname", "2005-05-05");

            Group G = new Group(5);

            G[0] = S1;
            G[1] = S2;
            G[2] = S3;
            G[3] = S4;
            G[4] = S5;

            PrintGroupDetails(G);

            //Console.WriteLine(S.firstName + S.lastName + S.birthDate);
            //Console.WriteLine(G[0].firstName + G[0].lastName + G[0].birthDate);
            Console.ReadKey();
        }
    }
}