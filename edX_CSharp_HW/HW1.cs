﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edX_CSharp_HW
{
    class HW1
    {
        static void Main(string[] args)
        {
            string studentFirstName = "Andrew";
            string studentLastName = "LastName";
            DateTime studentBirthDate = new DateTime(1986,5,22);
            string studentAddressLine1 = "AddressLine1";
            string studentAddressLine2 = "AddressLine1";
            string studentCity = "Teh City";
            string studentStateProvince = "Province";
            int studentZipPostal = 111;
            string studentCountry = "Nibiru";

            string professorInformation = "Professor Information";
            string studentUniversityDegree = "Bachelor";
            string studentUniversityProgram = "Computer Science and Engineering";
            string courseInformation = "Course Information";

            Console.WriteLine("Student First Name: " + studentFirstName);
            Console.WriteLine("Student Last Name: " + studentLastName);
            Console.WriteLine("Student Birth Date: " + studentBirthDate.ToString("dd.MM.yyyy"));
            Console.WriteLine("Student Address Line 1: " + studentAddressLine1);
            Console.WriteLine("Student Address Line 2: " + studentAddressLine2);
            Console.WriteLine("Student City: " + studentCity);
            Console.WriteLine("Student State/Province: " + studentStateProvince);
            Console.WriteLine("Student Zip/Postal: " + studentZipPostal);
            Console.WriteLine("Student Country: " + studentCountry);
            Console.WriteLine("Professor Information: " + professorInformation);
            Console.WriteLine("University Degree: " + studentUniversityDegree);
            Console.WriteLine("University Program: " + studentUniversityProgram);
            Console.WriteLine("Course Information: " + courseInformation);

            //Added so console window won't close
            Console.ReadLine();
        }
    }
}
