﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edX_CSharp_HW.HW5 {
    class Student {
        public static int count { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public DateTime birthDate { get; set; }

        public Student() {//adds instance counter to default constructor
            count++;
        }

        public Student(string fName, string lName, string bDate) : this() {//this() means use default constructor first, so no need to count++ here again
            firstName = fName;
            lastName = lName;
            DateTime dt = new DateTime();

            try {
                if (!DateTime.TryParse(bDate, out dt)) throw new FormatException();
            }
            catch (FormatException fe) {
                Console.WriteLine(fe.Message + " Birth date was not succesfully parsed.");
            }

            birthDate = dt;
        }

        ~Student() { //when garbage collector calls destructor - decrement from number of instances
            count--;
        }
    }
}
