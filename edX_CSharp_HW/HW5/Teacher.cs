﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edX_CSharp_HW.HW5 {
    class Teacher {
        public static int count { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string college { get; set; }
        public string email { get; set; }
        public string phone { get; set; }

        public Teacher() {
            count++;
        }

        public Teacher(string fName, string lName, string coll, string mail, string ph) : this() {
            firstName = fName;
            lastName = lName;
            college = coll;
            email = mail;
            phone = ph;
        }

        ~Teacher() {
            count--;
        }
    }
}
