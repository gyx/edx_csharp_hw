﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edX_CSharp_HW.HW5 {
    class HW5 {
        static void Main(string[] args) {
            //UProgram IT = new UProgram("The Information technology program", true); //funny 1 string reversal because of instancing order from bottom to up
            //
            //Assignment
            //1.Instantiate three Student objects.
            Student S1 = new Student("Student1name", "Student1surname", "2001-01-01");
            Student S2 = new Student("Student2name", "Student2surname", "2002-02-02");
            Student S3 = new Student("Student3name", "Student3surname", "2003-03-03");
            //2.Instantiate a Course object called Programming with C#.
            Course course = new Course("Programming with C#", "edX", "DEV204x", 10, 400M);
            //3.Add your three students to this Course object.
            course.students = new Student[] {S1,S2,S3};
            //4.Instantiate at least one Teacher object.
            Teacher T1 = new Teacher("Teacher1name", "Teacher1surname", "Teacher1college", "Teacher1email", "Teacher1phone");
            //5.Add that Teacher object to your Course object
            course.teachers = new Teacher[] { T1 };
            //6.Instantiate a Degree object, such as Bachelor.
            Degree degree = new Degree("The Bachelor of Science degree", "BS", 30);
            //7.Add your Course object to the Degree object.
            degree.courses = new Course[] { course };
            //8.Instantiate a UProgram object called Information Technology.
            UProgram IT = new UProgram("The Information technology program");
            //9.Add the Degree object to the UProgram object.
            IT.degrees = new Degree[] { degree };
            //10.Using Console.WriteLine statements, output the following information to the console window:
            //-The name of the program and the degree it contains
            Console.WriteLine("{0} contains {1}", IT.name, IT.degrees[0].degreeName);
            Console.WriteLine();
            //-The name of the course in the degree
            Console.WriteLine("{0} contains the course {1}", IT.degrees[0].degreeName, IT.degrees[0].courses[0].courseName);
            Console.WriteLine();
            //-The count of the number of students in the course.
            Console.WriteLine("The {0} contains {1} student(s)", IT.degrees[0].courses[0].courseName, Student.count);
            Console.WriteLine();
        }
    }
}
