﻿//HW5.cs file
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edX_CSharp_HW.HW5 {
    class HW5 {
        static void Main(string[] args) {
            //UProgram IT = new UProgram("The Information technology program", true); //funny 1 string reversal because of instancing order from bottom to up
            //
            //Assignment
            //1.Instantiate three Student objects.
            Student S1 = new Student("Student1name", "Student1surname", "2001-01-01");
            Student S2 = new Student("Student2name", "Student2surname", "2002-02-02");
            Student S3 = new Student("Student3name", "Student3surname", "2003-03-03");
            //2.Instantiate a Course object called Programming with C#.
            Course course = new Course("Programming with C#", "edX", "DEV204x", 10, 400M);
            //3.Add your three students to this Course object.
            course.students = new Student[] { S1, S2, S3 };
            //4.Instantiate at least one Teacher object.
            Teacher T1 = new Teacher("Teacher1name", "Teacher1surname", "Teacher1college", "Teacher1email", "Teacher1phone");
            //5.Add that Teacher object to your Course object
            course.teachers = new Teacher[] { T1 };
            //6.Instantiate a Degree object, such as Bachelor.
            Degree degree = new Degree("The Bachelor of Science degree", "BS", 30);
            //7.Add your Course object to the Degree object.
            degree.courses = new Course[] { course };
            //8.Instantiate a UProgram object called Information Technology.
            UProgram IT = new UProgram("The Information technology program");
            //9.Add the Degree object to the UProgram object.
            IT.degrees = new Degree[] { degree };
            //10.Using Console.WriteLine statements, output the following information to the console window:
            //-The name of the program and the degree it contains
            Console.WriteLine("{0} contains {1}", IT.name, IT.degrees[0].degreeName);
            Console.WriteLine();
            //-The name of the course in the degree
            Console.WriteLine("{0} contains the course {1}", IT.degrees[0].degreeName, IT.degrees[0].courses[0].courseName);
            Console.WriteLine();
            //-The count of the number of students in the course.
            Console.WriteLine("The {0} contains {1} student(s)", IT.degrees[0].courses[0].courseName, Student.count);
            Console.WriteLine();
        }
    }

    //Course.cs file
    class Course {
        public static int count { get; set; }
        public string courseName { get; set; }
        public string collegeName { get; set; }
        public string courseAbbreviation { get; set; }
        public int courseCredits { get; set; }
        public decimal courseCost { get; set; }
        public Student[] students { get; set; }
        public Teacher[] teachers { get; set; }

        public Course() {
            count++;
        }

        public Course(string cName, string colName, string cAbb, int cCred, decimal cCost) : this() {
            courseName = cName;
            collegeName = colName;
            courseAbbreviation = cAbb;
            courseCredits = cCred;
            courseCost = cCost;

            //students = new Student[3];
            //teachers = new Teacher[3];
        }

        public Course(string cName, string colName, string cAbb, int cCred, decimal cCost, bool auto) : this() {
            courseName = cName;
            collegeName = colName;
            courseAbbreviation = cAbb;
            courseCredits = cCred;
            courseCost = cCost;

            Student S1 = new Student("Student1name", "Student1surname", "2001-01-01");
            Student S2 = new Student("Student2name", "Student2surname", "2002-02-02");
            Student S3 = new Student("Student3name", "Student3surname", "2003-03-03");

            students = new Student[] { S1, S2, S3 };

            Teacher T1 = new Teacher("Teacher1name", "Teacher1surname", "Teacher1college", "Teacher1email", "Teacher1phone");
            Teacher T2 = new Teacher("Teacher2name", "Teacher2surname", "Teacher2college", "Teacher2email", "Teacher2phone");
            Teacher T3 = new Teacher("Teacher3name", "Teacher3surname", "Teacher3college", "Teacher3email", "Teacher3phone");

            teachers = new Teacher[] { T1, T2, T3 };

            Output();
        }

        public void Output() {
            Console.WriteLine("{0} contains {1} student(s) and {2} teacher(s)", courseName, students.Length, teachers.Length);
            Console.WriteLine();
        }

        ~Course() {
            count--;
        }
    }

    //Degree.cs file
    class Degree {
        public static int count { get; set; }
        public string degreeName { get; set; }
        public string degreeAbbreviation { get; set; }
        public int degreeCredits { get; set; }
        public Course[] courses { get; set; }

        public Degree() {
            count++;
        }

        public Degree(string dName, string dAbb, int dCred) : this() {
            degreeName = dName;
            degreeAbbreviation = dAbb;
            degreeCredits = dCred;
        }

        public Degree(string dName, string dAbb, int dCred, bool auto) : this() {
            degreeName = dName;
            degreeAbbreviation = dAbb;
            degreeCredits = dCred;

            Course C1 = new Course("Programming with C#","edX","DEV204x",10,400M,true);
            courses = new Course[] { C1 };
            Output(courses[0]);
        }

        public void Output(Course c) {
            Console.WriteLine("{0} contains the course {1}", degreeName, c.courseName);
            Console.WriteLine();
        }

        ~Degree() {
            count--;
        }
    }

    //Student.cs file
    class Student {
        public static int count { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public DateTime birthDate { get; set; }

        public Student() {//adds instance counter to default constructor
            count++;
        }

        public Student(string fName, string lName, string bDate) : this() {//this() means use default constructor first, so no need to count++ here again
            firstName = fName;
            lastName = lName;
            DateTime dt = new DateTime();

            try {
                if (!DateTime.TryParse(bDate, out dt)) throw new FormatException();
            }
            catch (FormatException fe) {
                Console.WriteLine(fe.Message + " Birth date was not succesfully parsed.");
            }

            birthDate = dt;
        }

        ~Student() { //when garbage collector calls destructor - decrement from number of instances
            count--;
        }
    }

    //Teacher.cs file
    class Teacher {
        public static int count { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string college { get; set; }
        public string email { get; set; }
        public string phone { get; set; }

        public Teacher() {
            count++;
        }

        public Teacher(string fName, string lName, string coll, string mail, string ph) : this() {
            firstName = fName;
            lastName = lName;
            college = coll;
            email = mail;
            phone = ph;
        }

        ~Teacher() {
            count--;
        }
    }

    //Uprogram.cs file
    class UProgram {
        public static int count { get; set; }
        public string name { get; set; }
        public Degree[] degrees { get; set; }
        
        public UProgram(){
            count++;
        }

        public UProgram(string nm) : this() {
            name = nm;
        }

        public UProgram(string nm, bool auto) : this() {
            name = nm;

            Degree D1 = new Degree("The Bachelor of Science degree", "BS", 30, true);
            degrees = new Degree[] { D1 };
            Output(degrees[0]);
            
        }

        public void Output(Degree dg) {
            Console.WriteLine("{0} contains {1}", name, dg.degreeName);
            Console.WriteLine();
        }

        ~UProgram() {
            count--;
        }
    }
}
