﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edX_CSharp_HW.HW5 {
    class Course {
        public static int count { get; set; }
        public string courseName { get; set; }
        public string collegeName { get; set; }
        public string courseAbbreviation { get; set; }
        public int courseCredits { get; set; }
        public decimal courseCost { get; set; }
        public Student[] students { get; set; }
        public Teacher[] teachers { get; set; }

        public Course() {
            count++;
        }

        public Course(string cName, string colName, string cAbb, int cCred, decimal cCost) : this() {
            courseName = cName;
            collegeName = colName;
            courseAbbreviation = cAbb;
            courseCredits = cCred;
            courseCost = cCost;

            //students = new Student[3];
            //teachers = new Teacher[3];
        }

        public Course(string cName, string colName, string cAbb, int cCred, decimal cCost, bool auto) : this() {
            courseName = cName;
            collegeName = colName;
            courseAbbreviation = cAbb;
            courseCredits = cCred;
            courseCost = cCost;

            Student S1 = new Student("Student1name", "Student1surname", "2001-01-01");
            Student S2 = new Student("Student2name", "Student2surname", "2002-02-02");
            Student S3 = new Student("Student3name", "Student3surname", "2003-03-03");

            students = new Student[] { S1, S2, S3 };

            Teacher T1 = new Teacher("Teacher1name", "Teacher1surname", "Teacher1college", "Teacher1email", "Teacher1phone");
            Teacher T2 = new Teacher("Teacher2name", "Teacher2surname", "Teacher2college", "Teacher2email", "Teacher2phone");
            Teacher T3 = new Teacher("Teacher3name", "Teacher3surname", "Teacher3college", "Teacher3email", "Teacher3phone");

            teachers = new Teacher[] { T1, T2, T3 };

            Output();
        }

        public void Output() {
            Console.WriteLine("{0} contains {1} student(s) and {2} teacher(s)", courseName, students.Length, teachers.Length);
            Console.WriteLine();
        }

        ~Course() {
            count--;
        }
    }
}
