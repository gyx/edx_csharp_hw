﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace edX_CSharp_HW
{
    class HW3
    {
        // At least three attributes each
        static void GetStudentInfo() {
            Console.WriteLine("Enter the student's first name: ");
            string firstName = Console.ReadLine();
            
            Console.WriteLine("Enter the student's last name");
            string lastName = Console.ReadLine();
            
            // Code to finish getting the rest of the student data

            Console.WriteLine("Enter the student's birthday (yyyy.mm.dd, dd/mm/yy etc)");
            string input = Console.ReadLine();
            DateTime birthDate = new DateTime();

            // try/catch experiments
            try {
                if (!DateTime.TryParse(input, out birthDate)) throw new FormatException();
            }
            catch (NullReferenceException nre) {
                // Catch all NullReferenceException exceptions.
                Console.WriteLine(nre.Message);
            }
            catch (FormatException fe) {
                // Catch all NullReferenceException exceptions.
                Console.WriteLine(fe.Message + " Birth date was not succesfully parsed.");
            }
            catch (Exception e)
            {
                // Attempt to handle the exception
                Console.WriteLine(e.Message);
                // If this catch handler cannot resolve the exception, 
                // throw it to the calling code
            }
            finally
            {
                // Code that always runs to close files or release resources.
            }

            PrintStudentDetails(firstName, lastName, birthDate);
        }

        static void PrintStudentDetails(string first, string last, DateTime birthDate)
        {
            Console.WriteLine("{0} {1} was born on: {2}", first, last, birthDate.ToString("yyyy.MM.dd"));
            Console.WriteLine();
        }

        static void GetTeacherInfo()
        {
            Console.WriteLine("Enter the teacher's first name: ");
            string firstName = Console.ReadLine();
            Console.WriteLine("Enter the teacher's last name");
            string lastName = Console.ReadLine();
            Console.WriteLine("Enter the teacher's college");
            string college = Console.ReadLine();
            Console.WriteLine("Enter the teacher's email");
            string email = Console.ReadLine();
            Console.WriteLine("Enter the teacher's number");
            string phone = Console.ReadLine();

            PrintTeacherDetails(firstName, lastName, college, email, phone);
        }

        static void PrintTeacherDetails(string first, string last, string college, string email, string phone)
        {
            Console.WriteLine("{0} {1} from {2}, email: {3}, phone: {4}", first, last, college, email, phone);
            Console.WriteLine();
        }

        static void GetCourseInfo()
        {
            Console.WriteLine("Enter course name: ");
            string courseName = Console.ReadLine();
            Console.WriteLine("Enter college name");
            string collegeName = Console.ReadLine();
            Console.WriteLine("Enter course abbreviation");
            string courseAbbreviation = Console.ReadLine();
            Console.WriteLine("Enter course credits");
            int courseCredits = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter course cost");
            decimal courseCost = Convert.ToDecimal(Console.ReadLine(), CultureInfo.InvariantCulture);

            PrintCourseDetails(courseName, collegeName, courseAbbreviation, courseCredits, courseCost);
        }

        static void PrintCourseDetails(string courseName, string collegeName, string courseAbbreviation, int courseCredits, decimal courseCost)
        {
            Console.WriteLine("{0} in {1} abbreviated {2} with {3} credits for as little as {4}", courseName, collegeName, courseAbbreviation, courseCredits, courseCost);
            Console.WriteLine();
        }

        static void GetDegreeInfo()
        {
            Console.WriteLine("Enter degree name: ");
            string degreeName = Console.ReadLine();
            Console.WriteLine("Enter degree abbreviation");
            string degreeAbbreviation = Console.ReadLine();
            Console.WriteLine("Enter number of degree credits required");
            int degreeCredits = Convert.ToInt32(Console.ReadLine());

            PrintDegreeDetails(degreeName, degreeAbbreviation, degreeCredits);
        }

        static void PrintDegreeDetails(string degreeName, string degreeAbbreviation, int degreeCredits)
        {
            Console.WriteLine("Degree name: {0}, Abbreviation: {1}, Credits required: {2}", degreeName, degreeAbbreviation, degreeCredits);
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            GetStudentInfo();
            GetTeacherInfo();
            GetCourseInfo();
            GetDegreeInfo();

            Console.Write("Press any key to continue");
            Console.ReadKey();

            // as per requirement
            try
            {
                Console.WriteLine("Throwing NotImplementedException");
                throw new NotImplementedException();
            }
            catch (NotImplementedException nie)
            {
                Console.WriteLine(nie.Message);
                Console.ReadKey();
            }
        }
    }
}