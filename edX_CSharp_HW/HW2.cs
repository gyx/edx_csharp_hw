﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edX_CSharp_HW
{
    class HW2
    {
        static void Main(string[] args)
        {
            int reverseToken = 1;
            for (int i = 1; i < 65; i++)
            {
                if (((i % 2 == 1) && (reverseToken > 0)) || ((i % 2 == 0) && (reverseToken < 0)))
                {
                    Console.Write("X");
                }
                else if (((i % 2 == 0) && (reverseToken > 0)) || ((i % 2 == 1) && (reverseToken < 0)))
                {
                    Console.Write("O");
                }

                if (i % 8 == 0)
                {
                    reverseToken *= -1;
                    Console.WriteLine();
                }
            }
        }
    }
}