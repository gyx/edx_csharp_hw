﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edX_CSharp_HW.HW6 {
    class Student : Person {
        public static int count { get; set; }

        public Student() {//adds instance counter to default constructor
            count++;
        }

        public Student(string fName, string lName, string bDate) : this() {//this() means use default constructor first, so no need to count++ here again
            firstName = fName;
            lastName = lName;
            DateTime dt = new DateTime();

            try {
                if (!DateTime.TryParse(bDate, out dt)) throw new FormatException();
            }
            catch (FormatException fe) {
                Console.WriteLine(fe.Message + " Birth date was not succesfully parsed.");
            }

            birthDate = dt;
        }

        ~Student() { //when garbage collector calls destructor - decrement from number of instances
            count--;
        }

        public string TakeTest() {
            Random rnd = new Random();

            int length = rnd.Next(1, 21);

            char [] test = new char[length];
            for ( int i = 0; i < length; i++ ) {
                test[i] = (char)rnd.Next ('a', 'z');
            }
            
            return test.ToString();
        }
    }
}
