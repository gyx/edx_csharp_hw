﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edX_CSharp_HW.HW6 {
    class UProgram {
        public static int count { get; set; }
        public string name { get; set; }
        public Degree[] degrees { get; set; }
        
        public UProgram(){
            count++;
        }

        public UProgram(string nm) : this() {
            name = nm;
        }

        public UProgram(string nm, bool auto) : this() {
            name = nm;

            Degree D1 = new Degree("The Bachelor of Science degree", "BS", 30, true);
            degrees = new Degree[] { D1 };
            Output(degrees[0]);
            
        }

        public void Output(Degree dg) {
            Console.WriteLine("{0} contains {1}", name, dg.degreeName);
            Console.WriteLine();
        }

        ~UProgram() {
            count--;
        }
    }
}
