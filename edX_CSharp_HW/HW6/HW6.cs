﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edX_CSharp_HW.HW6 {
    class HW6 {
                //HW6
                //1.Create a Person base class with common attributes for a person
                //2.Make your Student and Teacher classes inherit from the Person base class
                //3.Modify your Student and Teacher classes so that they inherit the common attributes from Person
                //4.Modify your Student and Teacher classes so they include characteristics specific to their type.  For example, a Teacher object might have a GradeTest() method where a student might have a TakeTest() method.
                //5.Run the same code in Program.cs from Module 5 to create instances of your classes so that you can setup a single course that is part of a program and a degree path.   Be sure to include at least one Teacher and an array of Students.
                //6.Ensure the Console.WriteLine statements you included in Homework 5, still output the correct information.

        static void Main(string[] args) {//HW5
            //1.Instantiate three Student objects.
            Student S1 = new Student("Student1name", "Student1surname", "2001-01-01");
            Student S2 = new Student("Student2name", "Student2surname", "2002-02-02");
            Student S3 = new Student("Student3name", "Student3surname", "2003-03-03");
            //2.Instantiate a Course object called Programming with C#.
            Course course = new Course("Programming with C#", "edX", "DEV204x", 10, 400M);
            //3.Add your three students to this Course object.
            course.students = new Student[] { S1, S2, S3 };
            //4.Instantiate at least one Teacher object.
            Teacher T1 = new Teacher("Teacher1name", "Teacher1surname", "Teacher1college", "Teacher1email", "Teacher1phone");
            //5.Add that Teacher object to your Course object
            course.teachers = new Teacher[] { T1 };
            //6.Instantiate a Degree object, such as Bachelor.
            Degree degree = new Degree("The Bachelor of Science degree", "BS", 30);
            //7.Add your Course object to the Degree object.
            degree.courses = new Course[] { course };
            //8.Instantiate a UProgram object called Information Technology.
            UProgram IT = new UProgram("The Information technology program");
            //9.Add the Degree object to the UProgram object.
            IT.degrees = new Degree[] { degree };
            //10.Using Console.WriteLine statements, output the following information to the console window:
            //-The name of the program and the degree it contains
            Console.WriteLine("{0} contains {1}", IT.name, IT.degrees[0].degreeName);
            Console.WriteLine();
            //-The name of the course in the degree
            Console.WriteLine("{0} contains the course {1}", IT.degrees[0].degreeName, IT.degrees[0].courses[0].courseName);
            Console.WriteLine();
            //-The count of the number of students in the course.
            Console.WriteLine("The {0} contains {1} student(s)", IT.degrees[0].courses[0].courseName, Student.count);
            Console.WriteLine();
        }
    }
}
