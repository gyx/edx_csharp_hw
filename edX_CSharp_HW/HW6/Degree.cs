﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edX_CSharp_HW.HW6 {
    class Degree {
        public static int count { get; set; }
        public string degreeName { get; set; }
        public string degreeAbbreviation { get; set; }
        public int degreeCredits { get; set; }
        public Course[] courses { get; set; }

        public Degree() {
            count++;
        }

        public Degree(string dName, string dAbb, int dCred) : this() {
            degreeName = dName;
            degreeAbbreviation = dAbb;
            degreeCredits = dCred;
        }

        public Degree(string dName, string dAbb, int dCred, bool auto) : this() {
            degreeName = dName;
            degreeAbbreviation = dAbb;
            degreeCredits = dCred;

            Course C1 = new Course("Programming with C#","edX","DEV204x",10,400M,true);
            courses = new Course[] { C1 };
            Output(courses[0]);
        }

        public void Output(Course c) {
            Console.WriteLine("{0} contains the course {1}", degreeName, c.courseName);
            Console.WriteLine();
        }

        ~Degree() {
            count--;
        }
    }
}
