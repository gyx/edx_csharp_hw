﻿//HW6.cs file
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edX_CSharp_HW.HW6 {
    class HW6 {
        //HW6
        //1.Create a Person base class with common attributes for a person
        //2.Make your Student and Teacher classes inherit from the Person base class
        //3.Modify your Student and Teacher classes so that they inherit the common attributes from Person
        //4.Modify your Student and Teacher classes so they include characteristics specific to their type.  For example, a Teacher object might have a GradeTest() method where a student might have a TakeTest() method.
        //5.Run the same code in Program.cs from Module 5 to create instances of your classes so that you can setup a single course that is part of a program and a degree path.   Be sure to include at least one Teacher and an array of Students.
        //6.Ensure the Console.WriteLine statements you included in Homework 5, still output the correct information.

        static void Main(string[] args) {//HW5
            //1.Instantiate three Student objects.
            Student S1 = new Student("Student1name", "Student1surname", "2001-01-01");
            Student S2 = new Student("Student2name", "Student2surname", "2002-02-02");
            Student S3 = new Student("Student3name", "Student3surname", "2003-03-03");
            //2.Instantiate a Course object called Programming with C#.
            Course course = new Course("Programming with C#", "edX", "DEV204x", 10, 400M);
            //3.Add your three students to this Course object.
            course.students = new Student[] { S1, S2, S3 };
            //4.Instantiate at least one Teacher object.
            Teacher T1 = new Teacher("Teacher1name", "Teacher1surname", "Teacher1college", "Teacher1email", "Teacher1phone");
            //5.Add that Teacher object to your Course object
            course.teachers = new Teacher[] { T1 };
            //6.Instantiate a Degree object, such as Bachelor.
            Degree degree = new Degree("The Bachelor of Science degree", "BS", 30);
            //7.Add your Course object to the Degree object.
            degree.courses = new Course[] { course };
            //8.Instantiate a UProgram object called Information Technology.
            UProgram IT = new UProgram("The Information technology program");
            //9.Add the Degree object to the UProgram object.
            IT.degrees = new Degree[] { degree };
            //10.Using Console.WriteLine statements, output the following information to the console window:
            //-The name of the program and the degree it contains
            Console.WriteLine("{0} contains {1}", IT.name, IT.degrees[0].degreeName);
            Console.WriteLine();
            //-The name of the course in the degree
            Console.WriteLine("{0} contains the course {1}", IT.degrees[0].degreeName, IT.degrees[0].courses[0].courseName);
            Console.WriteLine();
            //-The count of the number of students in the course.
            Console.WriteLine("The {0} contains {1} student(s)", IT.degrees[0].courses[0].courseName, Student.count);
            Console.WriteLine();
        }
    }


    //Course.cs file
    class Course {
        public static int count { get; set; }
        public string courseName { get; set; }
        public string collegeName { get; set; }
        public string courseAbbreviation { get; set; }
        public int courseCredits { get; set; }
        public decimal courseCost { get; set; }
        public Student[] students { get; set; }
        public Teacher[] teachers { get; set; }

        public Course() {
            count++;
        }

        public Course(string cName, string colName, string cAbb, int cCred, decimal cCost) : this() {
            courseName = cName;
            collegeName = colName;
            courseAbbreviation = cAbb;
            courseCredits = cCred;
            courseCost = cCost;

            students = new Student[3];
            teachers = new Teacher[3];
        }

        public Course(string cName, string colName, string cAbb, int cCred, decimal cCost, bool auto) : this() {
            courseName = cName;
            collegeName = colName;
            courseAbbreviation = cAbb;
            courseCredits = cCred;
            courseCost = cCost;

            Student S1 = new Student("Student1name", "Student1surname", "2001-01-01");
            Student S2 = new Student("Student2name", "Student2surname", "2002-02-02");
            Student S3 = new Student("Student3name", "Student3surname", "2003-03-03");

            students = new Student[] { S1, S2, S3 };

            Teacher T1 = new Teacher("Teacher1name", "Teacher1surname", "Teacher1college", "Teacher1email", "Teacher1phone");
            Teacher T2 = new Teacher("Teacher2name", "Teacher2surname", "Teacher2college", "Teacher2email", "Teacher2phone");
            Teacher T3 = new Teacher("Teacher3name", "Teacher3surname", "Teacher3college", "Teacher3email", "Teacher3phone");

            teachers = new Teacher[] { T1, T2, T3 };

            Output();
        }

        public void Output() {
            Console.WriteLine("{0} contains {1} student(s) and {2} teacher(s)", courseName, students.Length, teachers.Length);
            Console.WriteLine();
        }

        ~Course() {
            count--;
        }
    }

    //Degree.cs file
    class Degree {
        public static int count { get; set; }
        public string degreeName { get; set; }
        public string degreeAbbreviation { get; set; }
        public int degreeCredits { get; set; }
        public Course[] courses { get; set; }

        public Degree() {
            count++;
        }

        public Degree(string dName, string dAbb, int dCred) : this() {
            degreeName = dName;
            degreeAbbreviation = dAbb;
            degreeCredits = dCred;
        }

        public Degree(string dName, string dAbb, int dCred, bool auto) : this() {
            degreeName = dName;
            degreeAbbreviation = dAbb;
            degreeCredits = dCred;

            Course C1 = new Course("Programming with C#","edX","DEV204x",10,400M,true);
            courses = new Course[] { C1 };
            Output(courses[0]);
        }

        public void Output(Course c) {
            Console.WriteLine("{0} contains the course {1}", degreeName, c.courseName);
            Console.WriteLine();
        }

        ~Degree() {
            count--;
        }
    }

    //Person.cs file
    class Person {
        public static int count;
        public string firstName { get; set; }
        public string lastName { get; set; }
        public DateTime birthDate { get; set; }

        public Person() {
            count++;
        }

        public Person(string fName, string lName) : this() {
            firstName = fName;
            lastName = lName;
        }

        public Person(string fName, string lName, string bDate) : this() {
            firstName = fName;
            lastName = lName;

            DateTime dt = new DateTime();

            try {
                if (!DateTime.TryParse(bDate, out dt)) throw new FormatException();
            }
            catch (FormatException fe) {
                Console.WriteLine(fe.Message + " Birth date was not succesfully parsed.");
            }

            birthDate = dt;
        }


    }

    //Student.cs file
    class Student : Person {
        public static int count { get; set; }

        public Student() {//adds instance counter to default constructor
            count++;
        }

        public Student(string fName, string lName, string bDate) : this() {//this() means use default constructor first, so no need to count++ here again
            firstName = fName;
            lastName = lName;
            DateTime dt = new DateTime();

            try {
                if (!DateTime.TryParse(bDate, out dt)) throw new FormatException();
            }
            catch (FormatException fe) {
                Console.WriteLine(fe.Message + " Birth date was not succesfully parsed.");
            }

            birthDate = dt;
        }

        ~Student() { //when garbage collector calls destructor - decrement from number of instances
            count--;
        }

        public string TakeTest() {
            Random rnd = new Random();

            int length = rnd.Next(1, 21);

            char [] test = new char[length];
            for ( int i = 0; i < length; i++ ) {
                test[i] = (char)rnd.Next ('a', 'z');
            }
            
            return test.ToString();
        }
    }

    //Teacher.cs file
    class Teacher : Person {
        public static int count { get; set; }
        public string college { get; set; }
        public string email { get; set; }
        public string phone { get; set; }

        public Teacher() {
            count++;
        }

        public Teacher(string fName, string lName, string coll, string mail, string ph) : this() {
            firstName = fName;
            lastName = lName;
            college = coll;
            email = mail;
            phone = ph;
        }

        ~Teacher() {
            count--;
        }

        public string GradeTest(string test) {
            //wouldn't want him to grade anyone really)
            Random rnd = new Random();
            //generate evaluation based on random number between 1 and test length
            int eval = rnd.Next(1, test.Length);
            string grade = "F";

            switch (eval) {
                case 1:
                    grade = "E";
                    break;
                case 2:
                    grade = "D";
                    break;
                case 3:
                    grade = "C";
                    break;
                case 4:
                    grade = "B";
                    break;
                case 5:
                    grade = "A";
                    break;
                default:
                    grade = "F";
                    break;
            }

            return grade;
        }
    }

    //Uprogram.cs file
    class UProgram {
        public static int count { get; set; }
        public string name { get; set; }
        public Degree[] degrees { get; set; }
        
        public UProgram(){
            count++;
        }

        public UProgram(string nm) : this() {
            name = nm;
        }

        public UProgram(string nm, bool auto) : this() {
            name = nm;

            Degree D1 = new Degree("The Bachelor of Science degree", "BS", 30, true);
            degrees = new Degree[] { D1 };
            Output(degrees[0]);
            
        }

        public void Output(Degree dg) {
            Console.WriteLine("{0} contains {1}", name, dg.degreeName);
            Console.WriteLine();
        }

        ~UProgram() {
            count--;
        }
    }
}
