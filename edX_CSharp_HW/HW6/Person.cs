﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edX_CSharp_HW.HW6 {
    class Person {
        public static int count;
        public string firstName { get; set; }
        public string lastName { get; set; }
        public DateTime birthDate { get; set; }

        public Person() {
            count++;
        }

        public Person(string fName, string lName) : this() {
            firstName = fName;
            lastName = lName;
        }

        public Person(string fName, string lName, string bDate) : this() {
            firstName = fName;
            lastName = lName;

            DateTime dt = new DateTime();

            try {
                if (!DateTime.TryParse(bDate, out dt)) throw new FormatException();
            }
            catch (FormatException fe) {
                Console.WriteLine(fe.Message + " Birth date was not succesfully parsed.");
            }

            birthDate = dt;
        }


    }
}
