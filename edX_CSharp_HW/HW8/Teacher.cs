﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edX_CSharp_HW.HW8 {
    class Teacher : Person {
        public string college { get; set; }
        public string email { get; set; }
        public string phone { get; set; }

        public Teacher(string fName, string lName, string coll, string mail, string ph) : base(fName, lName) {
            college = coll;
            email = mail;
            phone = ph;
        }

        public string GradeTest(string test) {
            //wouldn't want him to grade anyone really)
            Random rnd = new Random();
            //generate evaluation based on random number between 1 and test length
            int eval = rnd.Next(1, test.Length);
            string grade = "F";

            switch (eval) {
                case 1:
                    grade = "E";
                    break;
                case 2:
                    grade = "D";
                    break;
                case 3:
                    grade = "C";
                    break;
                case 4:
                    grade = "B";
                    break;
                case 5:
                    grade = "A";
                    break;
                default:
                    grade = "F";
                    break;
            }

            return grade;
        }
    }
}
