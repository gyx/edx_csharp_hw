﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edX_CSharp_HW.HW8 {
    class UProgram {
        public string name { get; set; }
        public Degree[] degrees { get; set; }
        
        public UProgram(string nm) {
            name = nm;
        }

        public override string ToString() {
            return name + " contains  " + degrees[0].degreeName;
        }
    }
}
