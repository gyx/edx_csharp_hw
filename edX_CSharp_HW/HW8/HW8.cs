﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edX_CSharp_HW.HW8 {
    class HW8 {
        static void Main(string[] args) {/*HW8
For this assignment you will use your project from Module 7 and will use Generic collections in place of the existing collections.

1) Create a List<T>, of the proper data type, to replace the ArrayList and to hold students, inside the Course object.
2) Modify your code to use the List<T> collection as the replacement to the ArrayList. 
3) Create a Stack<T> object, of the proper data type, inside the Student object, called Grades, to store test scores.
4) Create 3 student objects.
5) Add 5 grades to the the Stack<T>  in the each Student object. (this does not have to be inside the constructor because you may not have grades for a student when you create a new student.)
6) Add the three Student objects to the List<T>  inside the Course object.
7) Using a foreach loop, iterate over the Students in the List<T> and output their first and last names to the console window. (For this exercise, casting is no longer required.  Also, place each student name on its own line)
8) Create a method inside the Course object called ListStudents() that contains the foreach loop.
9) Call the ListStudents() method from Main().
                                          
 * Grading Criteria:
1) Used a List<T> collection of the proper data type, inside the Course object.
2) Added a Stack<T> of the proper data type, called Grades, inside the Student object.
3) Added 3 Student objects to this List<T> using the List<T> method for adding objects.
4) Used a foreach loop to output the first and last name of each Student in the List<T>.*/
            
            Student S1 = new Student("Student1name", "Student1surname", "2001-01-01");
            Student S2 = new Student("Student2name", "Student2surname", "2002-02-02");
            Student S3 = new Student("Student3name", "Student3surname", "2003-03-03");

            S1.Grades.Push("A");
            S1.Grades.Push("B");
            S1.Grades.Push("C");
            S1.Grades.Push("D");
            S1.Grades.Push("E");

            S2.Grades.Push("A");
            S2.Grades.Push("B");
            S2.Grades.Push("C");
            S2.Grades.Push("D");
            S2.Grades.Push("E");

            S3.Grades.Push("A");
            S3.Grades.Push("B");
            S3.Grades.Push("C");
            S3.Grades.Push("D");
            S3.Grades.Push("E");

            Course course = new Course("Programming with C#", "edX", "DEV204x", 10, 400M);

            //Adding to ArrayList
            course.students.Add(S1);
            course.students.Add(S2);
            course.students.Add(S3);

            Teacher T1 = new Teacher("Teacher1name", "Teacher1surname", "Teacher1college", "Teacher1email", "Teacher1phone");

            course.teachers = new Teacher[] { T1 };

            Degree degree = new Degree("The Bachelor of Science degree", "BS", 30);

            degree.courses = new Course[] { course };

            UProgram IT = new UProgram("The Information technology program");

            IT.degrees = new Degree[] { degree };

            //output uses overrided ToString method that exists in every Object
            Console.WriteLine(IT);
            Console.WriteLine();

            Console.WriteLine(IT.degrees[0]);
            Console.WriteLine();

            Console.WriteLine(IT.degrees[0].courses[0]);
            Console.WriteLine();

            //Student list output
            IT.degrees[0].courses[0].getStudentList();
            Console.WriteLine();
        }
    }
}
