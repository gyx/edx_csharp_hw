﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edX_CSharp_HW.HW8 {
    class Degree {
        public string degreeName { get; set; }
        public string degreeAbbreviation { get; set; }
        public int degreeCredits { get; set; }
        public Course[] courses { get; set; }

        public Degree(string dName, string dAbb, int dCred) {
            degreeName = dName;
            degreeAbbreviation = dAbb;
            degreeCredits = dCred;
        }

        public override string ToString() {
            return degreeName + " contains the course " + courses[0].courseName;
        }
    }
}
